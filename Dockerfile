FROM mcr.microsoft.com/dotnet/core/aspnet:3.1-buster-slim AS base
RUN apt-get update \
    && apt-get install -y --allow-unauthenticated \
        libc6-dev \
        libgdiplus \
        libx11-dev \
     && rm -rf /var/lib/apt/lists/*
WORKDIR /app

FROM mcr.microsoft.com/dotnet/core/sdk:3.1-buster AS build
WORKDIR /src

COPY ["CICD.GitLab.Demo/CICD.GitLab.Demo.csproj", "CICD.GitLab.Demo/"]
RUN dotnet restore "CICD.GitLab.Demo/CICD.GitLab.Demo.csproj"
COPY . .

WORKDIR "/src/CICD.GitLab.Demo"
RUN dotnet build "CICD.GitLab.Demo.csproj" -c Release -o /app/build

FROM build AS publish
RUN dotnet publish "CICD.GitLab.Demo.csproj" -c Release -o /app/publish

FROM base AS final
WORKDIR /app
COPY --from=publish /app/publish .
ENTRYPOINT ["dotnet", "CICD.GitLab.Demo.dll"]
