﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CICD.GitLab.Demo.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class MessageController : ControllerBase
    {
        private readonly ILogger<MessageController> _logger;

        public MessageController(ILogger<MessageController> logger)
        {
            _logger = logger;
        }

        [HttpGet("/message/get", Name = "GetMessage")]
        public IActionResult GetMessage()
        {
            var message = "Hello world! Test merge from development!";
            _logger.LogDebug($"Return mesage: {message}");
            return Ok(message);
        }
    }
}
